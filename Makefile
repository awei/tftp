CC:=gcc
MAKE:=make

TFTP_DIR:=$(shell pwd)
TFTP_LIB_DIR:=$(TFTP_DIR)/lib
TFTPC_SRC_DIR:=$(TFTP_DIR)/tftpc/src
TFTPD_SRC_DIR:=$(TFTP_DIR)/tftpd/src

SYSTEM_BIN_DIR:=/usr/sbin
SYSTEM_LIB:=/lib

all:tftp_lib tftpc_src tftpd_src

clean:tftp_lib_clean tftpc_src_clean tftpd_src_clean

install:tftp_lib_install tftpc_src_install tftpd_src_install

uninstall:tftp_lib_uninstall tftpc_src_uninstall tftpd_src_uninstall

tftp_lib:
	$(MAKE) -C $(TFTP_LIB_DIR)
tftp_lib_clean:
	$(MAKE) -C $(TFTP_LIB_DIR) clean
tftp_lib_install:
	$(MAKE) -C $(TFTP_LIB_DIR) install
tftp_lib_uninstall:
	$(MAKE) -C $(TFTP_LIB_DIR) uninstall

tftpc_src:
	$(MAKE) -C $(TFTPC_SRC_DIR)
tftpc_src_clean:
	$(MAKE) -C $(TFTPC_SRC_DIR) clean
tftpc_src_install:
	$(MAKE) -C $(TFTPC_SRC_DIR) install
tftpc_src_uninstall:
	$(MAKE) -C $(TFTPC_SRC_DIR) uninstall

tftpd_src:
	$(MAKE) -C $(TFTPD_SRC_DIR)
tftpd_src_clean:
	$(MAKE) -C $(TFTPD_SRC_DIR) clean
tftpd_src_install:
	$(MAKE) -C $(TFTPD_SRC_DIR) install
tftpd_src_uninstall:
	$(MAKE) -C $(TFTPD_SRC_DIR) uninstall

export CC
export TFTP_LIB_DIR
export TFTPC_SRC_DIR
export TFTPD_SRC_DIR 
