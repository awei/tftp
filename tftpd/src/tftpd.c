#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>
#include <getopt.h>
#include <dirent.h>
#include <libgen.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>   
#include <netinet/in.h>  
#include <arpa/inet.h>  
#include "libtftp.h"

#define ADRESS_FLAG 1<<0
#define LOCAL_FLAG 1<<1
#define REMOTE_FLAG  1<<2
#define VERBOSE_FLAG 1<<3
#define DEBUG_FLAG 1<<4
#define PUT_FLAG 1<<5
#define GET_FLAG 1<<6
#define BINARY_FLAG 1<<7


#define TFTP_INIT_STATE	0
#define TFTP_READ_STATE	1
#define TFTP_WRITE_STATE	2
#define TFTP_DATA_STATE	3
#define TFTP_ACK_STATE	4
#define TFTP_ENC_ERROR_STATE 5
#define TFTP_DEC_ERROR_STATE 6
#define TFTP_END_STATE 7

  
#define VERSION "1.1"
#define BUF_SIZE 516
#define DATA_SIZE 512
#define FILE_SIZE 225
#define MSG_SIZE 225

int dbg_flag = 0;
int verbose_flag = 0;
int max_rrq_send = 5;
int max_wrq_send = 5;


struct variable
{
	int flag;
	char *mode;
	char *adress;
	char *rfilename;
	char *lfilename;
	
}var;

int num_cal(int file_size,int data_size)
{
	int t;
	float num;
	t = file_size / data_size + 1;
	num = (1 / (float)t)*100;
	return (int)num;
} 

void get_state_machine(int *sockfd,struct sockaddr_in addr,int addr_len,char *buffer,int state)
{
	int i = 0;
	int success = 100;
	int len;
	int size;
	int max_retry = 10;
	int final_flag = 0;
	FILE *fd;
	uint16_t opcode;
	uint16_t block = 0;
	uint16_t pblock = -1;
	uint16_t err_code = 0;
	char bar[102] = {0};
	char *lab = "-\\|/";
	char mode[32];
	char msg[MSG_SIZE];
	char err_msg[MSG_SIZE];
	char data[DATA_SIZE];
	char filename[FILE_SIZE];
	
	while(1)
	{
		switch(state)
		{
			case TFTP_WRITE_STATE:
				dec_wrq(buffer,filename,mode);
				bzero(buffer,BUF_SIZE);
				if((!filename)||(!mode))
				{
					err_code = ERR_ND;
					strcpy(err_msg,"Not defined");
					state = TFTP_ENC_ERROR_STATE;
				}
				else
				{
					if(strcmp(mode,"octet") == 0)
					{
						fd = fopen(filename,"wb");
					}
					else
					{
						fd = fopen(filename,"w");
					}
					if(!fd)
					{
						err_code = ERR_DF;
						strcpy(err_msg,"Disk full or allocation exceeded");
						state = TFTP_ENC_ERROR_STATE;
					}
					else 
					{
						state = TFTP_ACK_STATE;
					}
				}				
				break;
			case TFTP_DATA_STATE:
				block = dec_data(buffer,data,len);
				if(block <= 0)
				{
					err_code = ERR_ITO;
					strcpy(err_msg,"Illegal TFTP operation");
					state = TFTP_ENC_ERROR_STATE;
				}
				else
				{
					if(block == pblock + 1)
					{
						size = len - 4;
						if(fwrite(data,size,1,fd) != 1)
						{
							err_code = ERR_DF;
							strcpy(err_msg,"Disk full or allocation exceeded");
							state = TFTP_ENC_ERROR_STATE;
						};
						fflush(fd);
						bzero(data,DATA_SIZE);
						if(block == 1)
						{
							printf("[#");
						}
						if(block % 10 == 0)
							printf("#");
						state = TFTP_ACK_STATE;
					}
					else if(block < pblock + 1)
					{
						block = pblock;
						bzero(buffer,sizeof(buffer));
						bzero(data,sizeof(data));
						state = TFTP_ACK_STATE;
					}
				}
				break;
			case TFTP_ACK_STATE:
				len = enc_ack(buffer,block);
				sendto(*sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				if(final_flag == 1)
				{
					printf("][%d%%]\n",success);
					bzero(buffer,BUF_SIZE);
					state = TFTP_END_STATE;
					break;
				}
				pblock ++;
				bzero(buffer,BUF_SIZE);
				len = recvfrom(*sockfd,buffer,BUF_SIZE,0,(struct sockaddr *)&addr,&addr_len);
				opcode = get_opcode(buffer);
				if(opcode == TFTP_DATA)
				{
					if(len < BUF_SIZE)
					{
						final_flag = 1;
					}
					state = TFTP_DATA_STATE;
				}
				else if(opcode == TFTP_ERROR)
				{
					state = TFTP_DEC_ERROR_STATE;
				}
				else
				{	
					err_code = ERR_ITO;
					strcpy(err_msg,"Illegal TFTP operation");
					state = TFTP_ENC_ERROR_STATE;
				}
				break;
			case TFTP_ENC_ERROR_STATE:
				len = enc_error(buffer,err_code,err_msg);
				sendto(*sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				bzero(buffer,BUF_SIZE);
				state = TFTP_END_STATE;
				break;
			case TFTP_DEC_ERROR_STATE:
				err_code = dec_error(buffer,err_msg);
				printf("Opecode: Error Code (%d)\n",opcode);
				printf("Error code: (%d)\n",err_code);
				printf("Error message: %s\n",err_msg);
				state = TFTP_END_STATE;
				break;
			case TFTP_END_STATE:
			default:
				if(fd)
					fclose(fd);
				return;
		}
	}
get_exit:
	exit(0);	

}

void put_state_machine(int *sockfd,struct sockaddr_in addr,int addr_len,char *buffer,int state)
{
	int i = 0;
	int len;
	int num;
	int speed = 0;
	int size;
	int resend_flag = 0;
	int max_retry = 10;
	int final_flag = 0;
	int file_size = 0;
	FILE *fd;
	uint16_t opcode;
	uint16_t block = 1;
	uint16_t ack_block = 0;
	uint16_t err_code = 0;
	char bar[102] = {0};
	char *lab = "-\\|/";
	char mode[32];
	char err_msg[MSG_SIZE];
	char data[DATA_SIZE];
	char rebuffer[BUF_SIZE];
	char filename[FILE_SIZE];
	
	while(1)
	{
		switch(state)
		{
			case TFTP_READ_STATE:
				dec_rrq(buffer,filename,mode);
				bzero(buffer,BUF_SIZE);
				if((!filename)||(!mode))
				{
					err_code = ERR_ND;
					strcpy(err_msg,"Not defined");
					state = TFTP_ENC_ERROR_STATE;
				}
				else
				{
					if(strcmp(mode,"octet") == 0)
					{
						fd = fopen(filename,"rb");
					}
					else
					{
						fd = fopen(filename,"r");
					}
					if(!fd)
					{
						err_code = ERR_DF;
						strcpy(err_msg,"File not found");
						state = TFTP_ENC_ERROR_STATE;
					}
					else 
					{
						file_size = get_file_size(filename);
						num = num_cal(file_size,DATA_SIZE);
						state = TFTP_DATA_STATE;
					}
				}				
				break;
			case TFTP_DATA_STATE:
				if(resend_flag)
				{
					sendto(*sockfd,rebuffer,len,0,(struct sockaddr *)&addr,addr_len);
					resend_flag = 0;
				}
				else
				{		
					size = fread(data,1,DATA_SIZE,fd);
					if(size < DATA_SIZE)
					{
						final_flag = 1;
					}
					len = enc_data(buffer,block,data,size);
					bzero(rebuffer,BUF_SIZE);
					for(int i = 0; i < len; i++)
						rebuffer[i] = buffer[i];
					sendto(*sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
					bzero(buffer,BUF_SIZE);
					bzero(data,DATA_SIZE);
				}
				len = recvfrom(*sockfd,buffer,BUF_SIZE,0,(struct sockaddr *)&addr,&addr_len);
				opcode = get_opcode(buffer);
				
				if(opcode == TFTP_ACK)
				{
					speed = block * num;
					while(i <= speed)
					{
						printf("[%-101s][%d%%][%c]\r",bar,i,lab[i%4]);
						fflush(stdout);
						bar[i++] = '#';
						bar[i] = '\0';
					}
					state = TFTP_ACK_STATE;
				}
				else if(opcode == TFTP_ERROR)
				{
					state = TFTP_DEC_ERROR_STATE;
					
				}
				else
				{
					err_code = ERR_ITO;
					strcpy(err_msg,"Illegal TFTP operation");
					state = TFTP_ENC_ERROR_STATE;
				}
				break;
			case TFTP_ACK_STATE:
				ack_block = dec_ack(buffer);
				if(ack_block < 0)
				{
					err_code = ERR_ITO;
					strcpy(err_msg,"Illegal TFTP operation");
					state = TFTP_ENC_ERROR_STATE;
				}
				else 
				{
					if(ack_block == block)
					{
						if(final_flag == 1)
						{
							while(i <= 100)
							{
								printf("[%-101s][%d%%][%c]\r",bar,i,lab[i%4]);
								fflush(stdout);
								bar[i++] = '#';
								bar[i] = '\0';
							}
							printf("\n");
							state = TFTP_END_STATE;
							break;
						}
						else 
						{
							state = TFTP_DATA_STATE;
							block++;
						}
					}
					else if(ack_block == block - 1)
					{
						resend_flag = 1;
						state = TFTP_DATA_STATE;
					}
				}
				break;
			case TFTP_ENC_ERROR_STATE:
				len = enc_error(buffer,err_code,err_msg);
				sendto(*sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				bzero(buffer,BUF_SIZE);
				state = TFTP_END_STATE;
				break;
			case TFTP_DEC_ERROR_STATE:
				err_code = dec_error(buffer,err_msg);
				printf("Opecode: Error Code (%d)\n",opcode);
				printf("Error code: (%d)\n",err_code);
				printf("Error message: %s\n",err_msg);
				bzero(buffer,BUF_SIZE);
				state = TFTP_END_STATE;
				break;
			case TFTP_END_STATE:
			default:
				if(fd)
					fclose(fd);
				state = TFTP_INIT_STATE;
				return;
		}
	}
put_exit:
	exit(0);
}

int main(int argc,char *argv[])
{
	int len = 0;
	int sockfd;
	char addr_ip[20];
	uint16_t err_code;
	uint16_t opcode;
	char err_msg[MSG_SIZE];
	char buffer[BUF_SIZE];
	int state = TFTP_INIT_STATE;
	struct sockaddr_in addr;
	int addr_len =sizeof(struct sockaddr_in);
	

	if((sockfd = socket(AF_INET,SOCK_DGRAM,0))<0)
	{  
        perror("socket");  
        goto exit;
    }  
  
    bzero(&addr,sizeof(addr));  
    addr.sin_family = AF_INET;  
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(PORT);
	
	if(-1 == (bind(sockfd,(struct sockaddr *)&addr,sizeof(addr))))
	{
		printf("bind error!\n");
		goto sockfd_exit;
	}
	
	while(1)
	{
		len = recvfrom(sockfd,buffer,BUF_SIZE,0,(struct sockaddr *)&addr,&addr_len);
		if(len == -1)
		{
			bzero(buffer,BUF_SIZE);
			continue;
		}
		else 
		{
			opcode = get_opcode(buffer);
			if(opcode == TFTP_RRQ)
			{
				state = TFTP_READ_STATE;
				put_state_machine(&sockfd,addr,addr_len,buffer,state);
			}
			else if(opcode == TFTP_WRQ)
			{
				state = TFTP_WRITE_STATE;
				get_state_machine(&sockfd,addr,addr_len,buffer,state);
			}
			else 
			{
				bzero(buffer,BUF_SIZE);
				err_code = ERR_ITO;
				strcpy(err_msg,"Illegal TFTP operation");
				len = enc_error(buffer,err_code,err_msg);
				sendto(sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				bzero(buffer,BUF_SIZE);
			}
		}
	}		
sockfd_exit:
	close(sockfd);
exit:
	exit(EXIT_FAILURE);
}
