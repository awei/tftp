#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>
#include <getopt.h>
#include <dirent.h>
#include <libgen.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>   
#include <netinet/in.h>  
#include <arpa/inet.h>  
#include "libtftp.h"

#define ADRESS_FLAG 1<<0
#define LOCAL_FLAG 1<<1
#define REMOTE_FLAG  1<<2
#define VERBOSE_FLAG 1<<3
#define DEBUG_FLAG 1<<4
#define PUT_FLAG 1<<5
#define GET_FLAG 1<<6
#define BINARY_FLAG 1<<7


#define TFTP_INIT_STATE	0
#define TFTP_READ_STATE	1
#define TFTP_WRITE_STATE	2
#define TFTP_DATA_STATE	3
#define TFTP_ACK_STATE	4
#define TFTP_ENC_ERROR_STATE 5
#define TFTP_DEC_ERROR_STATE 6
#define TFTP_END_STATE 7

  
#define VERSION "1.1"
#define BUF_SIZE 516
#define DATA_SIZE 512
#define FILE_SIZE 225
#define MSG_SIZE 225


int dbg_flag = 0;
int verbose_flag = 0;
int max_rrq_send = 5;
int max_wrq_send = 5;


struct variable
{
	int flag;
	char *mode;
	char *adress;
	char *rfilename;
	char *lfilename;
	
}var;

void print_version()
{
	printf("version:%s\n",VERSION);
}
void usage(char *name)
{
	printf("Usage: %s Version: %s\n",name,VERSION);
	printf("\t-a hostaddress\n");
	printf("\t-D Debug\n");
	printf("\t-i binary(default netascii)\n");
	printf("\t-g get file\n");
	printf("\t-p put file\n");
	printf("\t-l localname\n");
	printf("\t-r remotename\n");
	printf("\t-V Verbose\n");
	printf("\t-v version\n");
	printf("\t-h help\n");
}

int num_cal(int file_size,int data_size)
{
	int t;
	float num;
	t = file_size / data_size + 1;
	num = (1 / (float)t)*100;
	return (int)num;
} 

int is_input_valid(struct variable *var)
{
	int ret = 1;
	if(!(var->flag & ADRESS_FLAG))
	{
		ret = 0;
		printf("-a parameter is require!\n");
		goto end;
	}
	
	if((var->flag & GET_FLAG)&&(var->flag & PUT_FLAG))
	{
		ret = 0;
		printf("can not use -p and -g at the same time!\n");
		goto end;
	}
	if(!((var->flag & GET_FLAG)||(var->flag & PUT_FLAG)))
	{
		ret = 0;
		printf("-p/-g parameter is require!\n");
		goto end;
	}
	if((var->flag & PUT_FLAG))
	{
		if(!(var->flag & LOCAL_FLAG))
		{
			ret = 0;
			printf("must use -p and -l at the same time!\n");
			goto end;
		}
		else if(!(var->flag & REMOTE_FLAG))
		{
			var->rfilename = var->lfilename;
		}
	}
	if((var->flag & GET_FLAG))
	{
		if(!(var->flag & REMOTE_FLAG))
		{
			ret = 0;
			printf("must use -g and -r at the same time!\n");
			goto end;
		}
		else if(!(var->flag & LOCAL_FLAG))
		{
			var->lfilename = var->rfilename;
		}
	}
	
end:
	return ret;
}

void get_state_machine(struct variable *var,int state)
{
	int i = 0;
	int len;
	int size;
	int sockfd;
	int success = 100;
	int max_retry = 10;
	int final_flag = 0;
	FILE *fd;
	uint16_t opcode;
	uint16_t block;
	uint16_t pblock = 0;
	uint16_t err_code = 0;
	char bar[102] = {0};
	char *lab = "-\\|/";
	char err_msg[MSG_SIZE];
	char buffer[BUF_SIZE];
	char data[DATA_SIZE];
	char filename[FILE_SIZE];
	struct sockaddr_in addr;
	int addr_len =sizeof(struct sockaddr_in);
	
	
	if((sockfd = socket(AF_INET,SOCK_DGRAM,0))<0)
	{  
        perror("socket");  
        goto get_exit;
    }  
  
    bzero(&addr,sizeof(addr));  
    addr.sin_family = AF_INET;  
    addr.sin_port = htons(PORT);  
    inet_pton(AF_INET,var->adress,&addr.sin_addr);
	
	while(1)
	{
		switch(state)
		{
			case TFTP_READ_STATE:
				len = enc_rrq(buffer,var->rfilename,var->mode);
				if(dbg_flag)
				{
					printf("rrq len = %d,mode = %s\n",len,var->mode);
				}
				sendto(sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				bzero(buffer,sizeof(buffer));
				max_rrq_send--;
				
recv_retry:			
				len = recvfrom(sockfd,buffer,BUF_SIZE,0,(struct sockaddr *)&addr,&addr_len);
				if(len<0)
				{
					max_retry--;
					if(max_retry > 0)
						goto recv_retry;
					else
					{
						if(max_rrq_send <= 0)
						{
							printf("Peer no response!\n");
							goto socket_exit;
						}					
					}
				}	
				else
				{
					opcode = get_opcode(buffer);
					if(opcode == TFTP_DATA)
					{
						if(var->flag & BINARY_FLAG)
						{
							fd = fopen(var->rfilename,"wb");
						}
						else
						{
							fd = fopen(var->rfilename,"w+");
						}
						if(!fd)
						{
							err_code = ERR_DF;
							strcpy(err_msg,"Disk full or allocation exceeded");
							state = TFTP_ENC_ERROR_STATE;
						}
						else 
						{
							if(len < BUF_SIZE)
							{
								final_flag = 1;
							}
							state = TFTP_DATA_STATE;
						}
					}
					else if(opcode == TFTP_ERROR)
					{	
						state = TFTP_DEC_ERROR_STATE;
					}
					else
					{
						err_code = ERR_ITO;
						strcpy(err_msg,"Illegal TFTP operation");
						state = TFTP_ENC_ERROR_STATE;	
					}
				}
				break;
			case TFTP_DATA_STATE:
				block = dec_data(buffer,data,len);
				if(block <= 0)
				{
					err_code = ERR_ITO;
					strcpy(err_msg,"Illegal TFTP operation");
					state = TFTP_ENC_ERROR_STATE;
				}
				else
				{
					if(block == pblock + 1)
					{
						size = len - 4;
						if(fwrite(data,size,1,fd) != 1)
						{
							err_code = ERR_DF;
							strcpy(err_msg,"Disk full or allocation exceeded");
							state = TFTP_ENC_ERROR_STATE;
						};
						fflush(fd);
						bzero(data,DATA_SIZE);
						if(block == 1)
						{
							printf("[#");
						}
						if(block % 10 == 0)
							printf("#");
						state = TFTP_ACK_STATE;
					}
					else if(block < pblock + 1)
					{
						block = pblock;
						bzero(buffer,sizeof(buffer));
						bzero(data,sizeof(data));
						state = TFTP_ACK_STATE;
					}
				}
				break;
			case TFTP_ACK_STATE:
				len = enc_ack(buffer,block);
				sendto(sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				if(final_flag == 1)
				{
					printf("][%d%%]\n",success);
					state = TFTP_END_STATE;
					break;
				}
				pblock ++;
				bzero(buffer,sizeof(buffer));
				len = recvfrom(sockfd,buffer,BUF_SIZE,0,(struct sockaddr *)&addr,&addr_len);
				opcode = get_opcode(buffer);
				if(opcode == TFTP_DATA)
				{
					if(len < BUF_SIZE)
					{
						final_flag = 1;
					}
					state = TFTP_DATA_STATE;
				}
				else if(opcode == TFTP_ERROR)
				{
					state = TFTP_DEC_ERROR_STATE;
				}
				else 
				{
					err_code = ERR_ITO;
					strcpy(err_msg,"Illegal TFTP operation");
					state = TFTP_ENC_ERROR_STATE;
				}
				break;
			case TFTP_ENC_ERROR_STATE:
				len = enc_error(buffer,err_code,err_msg);
				sendto(sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				bzero(buffer,BUF_SIZE);
				state = TFTP_END_STATE;
				break;
			case TFTP_DEC_ERROR_STATE:
				err_code = dec_error(buffer,err_msg);
				printf("Opecode: Error Code (%d)\n",opcode);
				printf("Error code: (%d)\n",err_code);
				printf("Error message: %s\n",err_msg);
				state = TFTP_END_STATE;
				break;
			case TFTP_END_STATE:
			default:
				if(sockfd)
					close(sockfd);
				if(fd)
					fclose(fd);
				exit(0);
		}
	}
socket_exit:
	close(sockfd);
get_exit:
	exit(0);	

}

void put_state_machine(struct variable *var,int state)
{
	int i = 0;
	int len;
	int num;
	int speed = 0;
	int size;
	int sockfd;
	int resend_flag = 0;
	int max_retry = 10;
	int final_flag = 0;
	int file_size = 0;
	FILE *fd;
	uint16_t opcode;
	uint16_t block = 0;
	uint16_t ack_block = 0;
	uint16_t err_code = 0;
	char bar[102] = {0};
	char *lab = "-\\|/";
	char msg[MSG_SIZE];
	char err_msg[MSG_SIZE];
	char buffer[BUF_SIZE];
	char data[DATA_SIZE];
	char rebuffer[BUF_SIZE];
	char filename[FILE_SIZE];
	
	struct sockaddr_in addr;
	int addr_len =sizeof(struct sockaddr_in);

	if((sockfd = socket(AF_INET,SOCK_DGRAM,0))<0)
	{  
        perror("socket");  
		goto put_exit;  
		
    }  
  
    bzero(&addr,sizeof(addr));  
    addr.sin_family = AF_INET;  
    addr.sin_port = htons(PORT);  
    inet_pton(AF_INET,var->adress,&addr.sin_addr);
	
	file_size = get_file_size(var -> lfilename);
	num = num_cal(file_size,DATA_SIZE);
	while(1)
	{
		switch(state)
		{
			case TFTP_WRITE_STATE:
				len = enc_wrq(buffer,var->lfilename,var->mode);
				if(dbg_flag)
				{
					printf("wrq len = %d,mode = %s\n",len,var->mode);
				}
				sendto(sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				bzero(buffer,sizeof(buffer));
				max_wrq_send--;
recv_more_time:
				len = recvfrom(sockfd,buffer,BUF_SIZE,0,(struct sockaddr *)&addr,&addr_len);
				if(len<0)
				{
					max_retry--;
					if(max_retry > 0)
						goto recv_more_time;
					else
					{
						if(max_wrq_send <=0 )
						{
							printf("peer no response\n");
							goto sockfd_exit;
						}	
					}
				}	
				else
				{
					opcode = get_opcode(buffer);
					if(opcode == TFTP_ACK)
					{
						if(var->flag & BINARY_FLAG)
						{
							fd = fopen(var->lfilename,"rb");
						}
						else
						{
							fd = fopen(var->lfilename,"r");
						}
						if(!fd)
						{
							err_code = ERR_DF;
							strcpy(err_msg,"Disk full or allocation exceeded");
							state = TFTP_ENC_ERROR_STATE;
						}
						else 
						{
							state = TFTP_ACK_STATE;
						}
					}
					else if(opcode == TFTP_ERROR)
					{	
						state = TFTP_DEC_ERROR_STATE;
					}
					else
					{
						err_code = ERR_ITO;
						strcpy(err_msg,"Illegal TFTP operation");
						state = TFTP_ENC_ERROR_STATE;
					}
				}
				break;
			case TFTP_DATA_STATE:
				if(resend_flag)
				{
					sendto(sockfd,rebuffer,len,0,(struct sockaddr *)&addr,addr_len);
					resend_flag = 0;
				}
				else
				{		
					size = fread(data,1,DATA_SIZE,fd);
					if(size < DATA_SIZE)
					{
						final_flag = 1;
					}
					len = enc_data(buffer,block,data,size);
					bzero(rebuffer,BUF_SIZE);
					for(int i = 0; i < len; i++)
						rebuffer[i] = buffer[i];
					sendto(sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
					bzero(buffer,BUF_SIZE);
					bzero(data,DATA_SIZE);
				}
				len = recvfrom(sockfd,buffer,BUF_SIZE,0,(struct sockaddr *)&addr,&addr_len);
				opcode = get_opcode(buffer);
				if(opcode == TFTP_ACK)
				{
					if(verbose_flag == 1)
					{
						speed = block * num;
						while(i <= speed)
						{
							printf("[%-101s][%d%%][%c]\r",bar,i,lab[i%4]);
							fflush(stdout);
							bar[i++] = '#';
							bar[i] = '\0';
						}
					}
					state = TFTP_ACK_STATE;
				}
				else if(opcode == TFTP_ERROR)
					{
						state = TFTP_DEC_ERROR_STATE;
					}
					else
					{
						err_code = ERR_ITO;
						strcpy(err_msg,"Illegal TFTP operation");
						state = TFTP_ENC_ERROR_STATE;
					}
				break;
			case TFTP_ACK_STATE:
				ack_block = dec_ack(buffer);
				if(ack_block < 0)
				{
					err_code = ERR_ITO;
					strcpy(err_msg,"Illegal TFTP operation");
					state = TFTP_ENC_ERROR_STATE;
				}
				else 
				{
					if(ack_block == block)
					{
						if(final_flag == 1)
						{
							if(verbose_flag == 1)
							{
								while(i <= 100)
								{
									printf("[%-101s][%d%%][%c]\r",bar,i,lab[i%4]);
									fflush(stdout);
									bar[i++] = '#';
									bar[i] = '\0';
								}
								printf("\n");
							}
							state = TFTP_END_STATE;
							break;
						}
						else 
						{
							state = TFTP_DATA_STATE;
							block++;
						}
					}
					else if(ack_block == block - 1)
					{
						resend_flag = 1;
						state = TFTP_DATA_STATE;
					}
				}
				break;
			case TFTP_ENC_ERROR_STATE:
				len = enc_error(buffer,err_code,err_msg);
				sendto(sockfd,buffer,len,0,(struct sockaddr *)&addr,addr_len);
				bzero(buffer,BUF_SIZE);
				state = TFTP_END_STATE;
				break;
			case TFTP_DEC_ERROR_STATE:
				err_code = dec_error(buffer,err_msg);
				printf("Opecode: Error Code (%d)\n",opcode);
				printf("Error code: (%d)\n",err_code);
				printf("Error message: %s\n",err_msg);
				state = TFTP_END_STATE;
				break;
			case TFTP_END_STATE:
			default:
				if(sockfd)
					close(sockfd);
				if(fd)
					fclose(fd);
				exit(0);
		}
	}
sockfd_exit:
	close(sockfd);
put_exit:
	exit(0);
}

int main(int argc,char *argv[])
{
	int opt;
	int state = TFTP_INIT_STATE;
	char mode[32] = "netascii";
	int option_index = 0;
	
	
	char *opt_string = "a:l:r:ipgvVDh";
	static struct option long_options[] =
    {  
		{"host", required_argument, NULL, 'a'},
		{"local", required_argument, NULL, 'l'},
		{"remote", required_argument, NULL, 'r'},
		{"get", no_argument, NULL, 'g'},
		{"put", no_argument, NULL, 'p'},
		{"binary", no_argument, NULL, 'i'},
		{"version", no_argument, NULL, 'v'},
		{"verbose", no_argument, NULL, 'V'},
		{"debug", no_argument, NULL, 'D'},
		{"help", no_argument, NULL, 'h'},
        {NULL, 0, NULL, 0},
    }; 

	
	while((opt = getopt_long(argc,argv,opt_string,long_options,&option_index)) != -1)
	{
		switch(opt)
		{
			case 'a':
				var.adress = optarg;
				var.flag |= ADRESS_FLAG;
				break;
			case 'i':
				var.flag |= BINARY_FLAG;
				break;
			case 'D':
				var.flag |= DEBUG_FLAG;
				break;
			case 'V':
				var.flag |= VERBOSE_FLAG;
				break;
			case 'g':
				var.flag |= GET_FLAG;
				break;
			case 'p':
				var.flag |= PUT_FLAG;
				break;
			case 'l':
				var.flag |= LOCAL_FLAG;
				var.lfilename = optarg;
				break;
			case 'r':
				var.flag |= REMOTE_FLAG;
				var.rfilename = optarg;
				break;
			case 'v':
				print_version();
				goto end;
			case 'h':
			case '?':
			default:
				usage(basename(argv[0]));
				goto end;
		}
	}	
	if(!is_input_valid(&var))
	{
		printf("error!\n");
		goto end;
	}
	if(var.flag & BINARY_FLAG)
	{
		bzero(mode,sizeof(mode));
		strcpy(mode,"octet");
	}
	if(var.flag & DEBUG_FLAG)
	{
		dbg_flag = 1;
	}
	var.mode = mode;
	printf("%s %s %s\n",var.lfilename,var.rfilename,var.adress);
	if(var.flag & VERBOSE_FLAG)
	{
		verbose_flag = 1;
	}
	if(var.flag & GET_FLAG)
	{
		state = TFTP_READ_STATE;
		get_state_machine(&var,state);
	}
	else if(var.flag & PUT_FLAG)
	{
		state = TFTP_WRITE_STATE;
		put_state_machine(&var,state);
	}
end:
	return 0;
}