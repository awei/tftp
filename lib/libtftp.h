#ifndef _LIB_TFTP_H
#define _LIB_TFTP_H

#define PORT 69

uint8_t TERM = 0x0;
enum opcode{TFTP_RRQ = 1,TFTP_WRQ,TFTP_DATA,TFTP_ACK,TFTP_ERROR};
enum error_code{ERR_ND = 0,ERR_FNF,ERR_AV,ERR_DF,ERR_ITO,ERR_UTID,ERR_FAE,ERR_NSU};

uint16_t get_opcode(char *buffer);
int get_file_size(char* filename);

int enc_rrq(char *buf,char *filename,char *mode);
int enc_wrq(char *buf,char *filename,char *mode);
int enc_ack(char *buf,uint16_t block);
int enc_data(char *buf,uint16_t block,char *data,int size);
int enc_error(char *buf,uint16_t error_code,char *err_msg);

void dec_rrq(char *buffer,char *filename,char *mode);
void dec_wrq(char *buffer,char *filename,char *mode);
uint16_t dec_data(char *buffer,char *data,int len);
uint16_t dec_ack(char *buffer);
uint16_t dec_error(char *buffer,char *err_msg);

#endif