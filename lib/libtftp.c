#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include "libtftp.h"

/*get opcode from a tftp packet*/
uint16_t get_opcode(char *buffer)
{
	uint16_t opcode;
	
	opcode = ntohs(*((uint16_t *)buffer));
	return opcode;
}

/*get file size*/
int get_file_size(char* filename) 
{ 
  struct stat statbuf; 
  stat(filename,&statbuf); 
  int size=statbuf.st_size; 
  
  return size; 
}

/*decrypt a tftp rq-type packet,mainly used for the function dec_rrq and dec_wrq*/
void dec_rq(char *buffer,char *filename,char *mode)
{
	int len;
	char *cp;
	
	cp = buffer + 2;
	len = strlen(cp);
	strcpy(filename,cp);

	cp += (len + 1);
	strcpy(mode,cp);
}

/*decrypt a tftp rrq packet*/
void dec_rrq(char *buffer,char *filename,char *mode)
{
	dec_rq(buffer,filename,mode);
}

/*decrypt a tftp wrq packet*/
void dec_wrq(char *buffer,char *filename,char *mode)
{
	dec_rq(buffer,filename,mode);
}

/*decrypt a tftp data packet*/
uint16_t dec_data(char *buffer,char *data,int len)
{
	char j;
	char *cp;
	uint16_t block;
	
	cp = buffer + 2;
	block = ntohs(*((uint16_t *)cp));
	memcpy(data,buffer+4,len);
	
	return block;
}

/*decrypt a tftp ack packet*/
uint16_t dec_ack(char *buffer)
{
	uint16_t block = 0;
	char *cp;
	
	cp = buffer + 2;
	block = ntohs(*((uint16_t *)cp));

	return block;
}

/*decrypt a tftp error packet*/
uint16_t dec_error(char *buffer,char *err_msg)
{
	
	char *cp;
	uint16_t err_code;
	
	cp = buffer + 2;
	err_code = ntohs(*((uint16_t *)cp));
	cp += 2;
	strcpy(err_msg,cp);
	
	return err_code;
}

/*encrypt a tftp rq-type packet,mainly used for function enc_rrq and enc_wrq*/
int enc_rq(char *buf,char *filename,char *mode,uint16_t opcode)
{
	char *cp;
	int len;

	cp = buf + 2;
	strcpy(cp,filename);
	cp = cp + strlen(filename);
	*((uint8_t *)cp) = TERM;
	cp++;
	stpcpy(cp,mode);
	cp = cp + strlen(mode);
	*((uint8_t *)cp) = TERM;
	cp++;
	*((uint16_t *)buf) = htons(opcode);
	
	len = cp - buf;
	return len;
}

/*encrypt a tftp rrq packet*/
int enc_rrq(char *buf,char *filename,char *mode)
{
	uint16_t opcode = TFTP_RRQ;
	return enc_rq(buf,filename,mode,opcode);
}

/*encrypt a tftp wrq packet*/
int enc_wrq(char *buf,char *filename,char *mode)
{
	uint16_t opcode = TFTP_WRQ;
	return enc_rq(buf,filename,mode,opcode);
}

/*encrypt a tftp ack packet*/
int enc_ack(char *buf,uint16_t block)
{
	uint16_t opcode = TFTP_ACK;
	char *cp;
	int len;
	
	cp = buf + 2;
	*((uint16_t *)cp) = htons(block);
	cp += 2;
	*((uint16_t *)buf) = htons(opcode);
	
	len = cp - buf;
	return len;
}

/*encrypt a tftp data packet*/
int enc_data(char *buf,uint16_t block,char *data,int size)
{
	int len;
	char *cp;
	
	uint16_t opcode = TFTP_DATA;
	
	cp = buf + 2;
	*((uint16_t *)cp) = htons(block);
	cp += 2;
	memcpy(cp,data,size);
	cp = cp + size;
	*((uint16_t *)buf) = htons(opcode);
	
	len = cp - buf;
	return len;
}

/*encrypt a tftp error packet*/
int enc_error(char *buf,uint16_t error_code,char *err_msg)
{
	char *cp;
	int len;
	uint16_t opcode = TFTP_ERROR;
	
	cp = buf + 2;
	*((uint16_t *)cp) = htons(error_code);
	cp += 2;
	stpcpy(cp,err_msg);
	cp = cp + strlen(err_msg);
	*((uint8_t *)cp) = TERM;
	cp++;
	*((uint16_t *)buf) = htons(opcode);
	
	len = cp - buf;
	
	return len;
}
